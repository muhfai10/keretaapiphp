<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active"><a href="?page=home"><i class="icon icon-home"></i> <span>Home</span></a> </li>
    <li> <a href="?page=pelanggan"><i class="icon icon-signal"></i> <span>Pelanggan</span></a> </li>
    <li> <a href="?page=tujuan"><i class="icon icon-inbox"></i> <span>Tujuan Kereta</span></a> </li>
    <li><a href="?page=pemesanan"><i class="icon icon-th"></i> <span>Pemesanan</span></a></li>
	 <li><a href="?page=kereta"><i class="icon icon-th"></i> <span>Kereta</span></a></li>
    <li><a href="?page=kelas"><i class="icon icon-fullscreen"></i> <span>Kelas Kereta</span></a></li>
    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Masinis</span></a>
      <ul>
        <li><a href="?page=petugas">Data Masinis</a></li>
        <li><a href="?page=pangkat">Pangkat Masinis</a></li>
      </ul>
    </li>
    <li><a href="logout.php"><i class="icon icon-tint"></i> <span>Logout</span></a></li>
  </ul>
</div>